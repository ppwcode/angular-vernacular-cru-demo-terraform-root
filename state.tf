module "tfstate" {
  source               = "github.com/peopleware/terraform-azure-ppwcode-tfstate?ref=1.0.0"
  storage_account_name = azurerm_storage_account.tfstate.name
  organisation_name    = "angular-vernacular-cru-demo"
}

locals {
  location = "West Europe"
}

resource "azurerm_resource_group" "tfstate" {
  name     = "tfstate"
  location = local.location
}

resource "azurerm_storage_account" "tfstate" {
  name                     = "tfstatengvernacularcru"
  resource_group_name      = azurerm_resource_group.tfstate.name
  location                 = local.location
  account_tier             = "Standard"
  account_replication_type = "GRS"
}
