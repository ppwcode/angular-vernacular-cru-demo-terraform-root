provider "azurerm" {
  features {}

  subscription_id = "ee80e5c6-b8c9-4c96-bdf9-da3113fcfe0c"
}

terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "=2.86.0"
    }
  }
}
